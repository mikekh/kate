# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2011, 2012, 2014, 2020, 2021, 2022, 2023.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2023-03-25 09:56+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, fuzzy, kde-format
#| msgid "GDB command"
msgid "GDB command"
msgstr "Polecenie GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Ścieżki wyszukiwania plików źródłowych"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Program miejscowy"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Zdalny TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Zdalny port szeregowy"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Gospodarz"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-bezwzględny-przedrostek"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-ścieżka-znajdywania"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Własne polecenia startowe"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Trwa sesja diagnostyczna. Uruchom ponownie, aby zatrzymać bieżącą sesję."

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Dodaj nowy cel"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Skopiuj cel"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Usuń cel"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Plik wykonywalny:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Katalog pracy:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "ID procesu:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumenty:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Zachowaj uaktywnienie"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Zachowaj uaktywnienie na wierszu poleceń"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Przekieruj WE/WY"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Przekieruj WE/WY diagnozowanych programów do osobnej karty"

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Ustawienia rozszerzone"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Cele"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Cel %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Settings"
msgid "Settings File:"
msgstr "Ustawienia"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr ""

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, fuzzy, kde-format
#| msgid "Debug"
msgid "Debugger"
msgstr "Diagnozuj"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Lokalne"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Rejestry CPU"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr "Ustaw plik wykonywalny na karcie 'Ustawienia' na pasku 'Diagnostyka'."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Nie wybrano debuggera. Wybierz go na karcie 'Ustawienia' na pasku "
"'Diagnostyka'."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Nie znaleziono debuggera. Upewnij się, że masz go na swoim systemie. Wybrany "
"debugger to '%1'"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Nie można uruchomić procesu diagnostycznego"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb zakończył zwyczajnie ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "wszystkie uruchomione wątki"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "uruchomione wątki: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "zatrzymany (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Bieżący wątek: %1 (zatrzymano wszystkie wątki)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Bieżący wątek: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Bieżąca ramka: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Gospodarz: %1. Cel: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Nie można przetworzyć ostatniej odpowiedzi: %1. Zbyt wiele błędów z "
"rzędu. Wyłączam."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Nie można przetworzyć ostatniej odpowiedzi: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "Niepowodzenie silnika DAP"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "program zakończył"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "żądanie rozłączenia"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "żądanie zamknięcia"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Silnik DAP: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Dotarto do pułapki:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(kontynuuj) wątek %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "wszystkie wątki kontynuowane"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(uruchomiony)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** zamknięto połączenie z serwerem ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "program zakończył z kodem wyjścia %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** oczekiwanie na działania użytkownika ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "błąd przy odpowiadaniu: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "ważna"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetria"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "diagnozowanie procesu [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "diagnozowanie procesu %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Metoda początkowa: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "wątek %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "ustawiono pułapkę"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "wyczyszczono pułapkę"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) pułapka"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<nieobliczone>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "możliwości serwera"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "obsługiwany"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "nieobsługiwane"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "pułapki warunkowe"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "pułapki funkcji"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "zatrzymaj na pułapkach warunkowych"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "punkty rejestrowane"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "żądania członów"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "żądanie przejścia do celów"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "zakończ żądanie"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "zakończ diagnozowanego"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "błąd składni: nie znaleziono wyrażenia"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "błąd składni: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "nieprawidłowy wiersz: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "nie określono pliku: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "nieprawidłowy id wątku: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "nie określono id wątku: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Dostępne polecenia:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "pułapki warunkowe nie są obsługiwane przez serwer"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""
"zatrzymywanie na pułapkach warunkowych nie jest obsługiwane przez serwer"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "wierz %1 ma już pułapkę"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "nie znaleziono pułapki (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Bieżący wątek: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "brak"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Bieżąca klatka: "

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Stan sesji: "

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "przygotowywanie"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "uruchomiony"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "zatrzymany"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "zakończone"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "rozłączony"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "nie znaleziono polecenia"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "brakuje id wątku"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "niszczenie silnika"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Bieżąca klatka [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Wartość"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "rodzaj"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "spisane rzeczy"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "nazwane rzeczy"

#: plugin_kategdb.cpp:106
#, fuzzy, kde-format
#| msgid "Kate GDB"
msgid "Kate Debug"
msgstr "Kate GDB"

#: plugin_kategdb.cpp:110
#, kde-format
msgid "Debug View"
msgstr "Widok diagnostyczny"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:343
#, kde-format
msgid "Debug"
msgstr "Diagnozuj"

#: plugin_kategdb.cpp:113 plugin_kategdb.cpp:116
#, kde-format
msgid "Locals and Stack"
msgstr "Lokalne i stos"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Ramka"

#: plugin_kategdb.cpp:200
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "GDB Output"
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Wyjście GDB"

#: plugin_kategdb.cpp:201
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Ustawienia"

#: plugin_kategdb.cpp:243
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Nie można otworzyć pliku:</title><nl/>%1<br/>Spróbuj dodać ścieżkę "
"wyszukiwania w Ustawienia rozszerzone -> Ścieżki wyszukiwania plików "
"źródłowych"

#: plugin_kategdb.cpp:268
#, kde-format
msgid "Start Debugging"
msgstr "Rozpocznij diagnozowanie"

#: plugin_kategdb.cpp:278
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Zakończ / Przestań diagnozować"

#: plugin_kategdb.cpp:285
#, kde-format
msgid "Continue"
msgstr "Kontynuuj"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Włącz pułapkę / przerwę"

#: plugin_kategdb.cpp:297
#, kde-format
msgid "Step In"
msgstr "Wejdź wgłąb"

#: plugin_kategdb.cpp:304
#, kde-format
msgid "Step Over"
msgstr "Przejdź ponad"

#: plugin_kategdb.cpp:311
#, kde-format
msgid "Step Out"
msgstr "Wyjdź stąd"

#: plugin_kategdb.cpp:318 plugin_kategdb.cpp:350
#, kde-format
msgid "Run To Cursor"
msgstr "Wykonaj do kursora"

#: plugin_kategdb.cpp:325
#, kde-format
msgid "Restart Debugging"
msgstr "Zdiagnozuj ponownie"

#: plugin_kategdb.cpp:333 plugin_kategdb.cpp:352
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Przenieś PC"

#: plugin_kategdb.cpp:338
#, kde-format
msgid "Print Value"
msgstr "Wyświetl wartość"

#: plugin_kategdb.cpp:347
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:349
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:431 plugin_kategdb.cpp:447
#, kde-format
msgid "Insert breakpoint"
msgstr "Wstaw pułapkę"

#: plugin_kategdb.cpp:445
#, kde-format
msgid "Remove breakpoint"
msgstr "Usuń pułapkę"

#: plugin_kategdb.cpp:599 plugin_kategdb.cpp:613
#, kde-format
msgid "Execution point"
msgstr "Punkt wykonania"

#: plugin_kategdb.cpp:771
#, kde-format
msgid "Thread %1"
msgstr "Wątek %1"

#: plugin_kategdb.cpp:871
#, kde-format
msgid "IO"
msgstr "WE/WY"

#: plugin_kategdb.cpp:956 plugin_kategdb.cpp:964
#, kde-format
msgid "Breakpoint"
msgstr "Pułapka"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Diagnozuj"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, fuzzy, kde-format
#| msgid "GDB Plugin"
msgid "Debug Plugin"
msgstr "Wtyczka GDB"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Łukasz Wojniłowicz"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lukasz.wojnilowicz@gmail.com"

#~ msgid "GDB Integration"
#~ msgstr "Integracja GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Integracja GDB z Kate"

#~ msgid "/dev/ttyUSB0"
#~ msgstr "/dev/ttyUSB0"

#~ msgid "9600"
#~ msgstr "9600"

#~ msgid "14400"
#~ msgstr "14400"

#~ msgid "19200"
#~ msgstr "19200"

#~ msgid "38400"
#~ msgstr "38400"

#~ msgid "57600"
#~ msgstr "57600"

#~ msgid "115200"
#~ msgstr "115200"

#~ msgid "&Target:"
#~ msgstr "&Cel:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "&Lista arg.:"

#~ msgid "Remove Argument List"
#~ msgstr "Usuń listę argumentów"

#~ msgid "Arg Lists"
#~ msgstr "Lista arg."
