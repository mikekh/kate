# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ignacio Poggi <ignaciop.3@gmail.com>, 2008.
# Eloy Cuadra <ecuadra@eloihr.net>, 2009, 2010, 2014, 2018, 2019, 2020, 2021, 2022, 2023.
# Cristina Yenyxe González García <the.blue.valkyrie@gmail.com>, 2011, 2012.
# Rocio Gallego <traducciones@rociogallego.com>, 2012, 2013, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-08 01:19+0000\n"
"PO-Revision-Date: 2023-04-20 04:46+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.03.90\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra,Rocío Gallego,Cristina Yenyxe González García"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"ecuadra@eloihr.net,traducciones@rociogallego.com,the.blue.valkyrie@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Salida"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Compilar de nuevo"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: plugin_katebuild.cpp:191 plugin_katebuild.cpp:198 plugin_katebuild.cpp:1162
#, kde-format
msgid "Build"
msgstr "Construir"

#: plugin_katebuild.cpp:201
#, kde-format
msgid "Select Target..."
msgstr "Seleccionar objetivo..."

#: plugin_katebuild.cpp:206
#, kde-format
msgid "Build Selected Target"
msgstr "Compilar el objetivo seleccionado"

#: plugin_katebuild.cpp:211
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Compilar y ejecutar el objetivo seleccionado"

#: plugin_katebuild.cpp:216
#, kde-format
msgid "Stop"
msgstr "Detener"

#: plugin_katebuild.cpp:221
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "Foco en siguiente pestaña a la izquierda"

#: plugin_katebuild.cpp:241
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "Foco en siguiente pestaña a la derecha"

#: plugin_katebuild.cpp:263
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Preferencias del objetivo"

#: plugin_katebuild.cpp:382
#, kde-format
msgid "Build Information"
msgstr "Información de la compilación"

#: plugin_katebuild.cpp:463 plugin_katebuild.cpp:1215 plugin_katebuild.cpp:1226
#: plugin_katebuild.cpp:1247 plugin_katebuild.cpp:1257
#, kde-format
msgid "Project Plugin Targets"
msgstr "Objetivos del complemento de proyecto"

#: plugin_katebuild.cpp:560
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "No se ha especificado un archivo o carpeta para compilar."

#: plugin_katebuild.cpp:564
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"El archivo «%1» no es local. Los archivos que no son locales no se pueden "
"compilar."

#: plugin_katebuild.cpp:611
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"No se puede ejecutar la orden: %1\n"
"La ruta de trabajo no existe: %2"

#: plugin_katebuild.cpp:625
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Fallo al ejecutar «%1». Estado de salida = %2"

#: plugin_katebuild.cpp:640
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Se ha cancelado la compilación de <b>%1</b>"

#: plugin_katebuild.cpp:747
#, kde-format
msgid "No target available for building."
msgstr "Ningún objetivo disponible para compilar"

#: plugin_katebuild.cpp:761
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "No se ha especificado un archivo o directorio local para compilar."

#: plugin_katebuild.cpp:767
#, kde-format
msgid "Already building..."
msgstr "Ya se está compilando."

#: plugin_katebuild.cpp:794
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Compilando objetivo <b>%1</b> ..."

#: plugin_katebuild.cpp:808
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Resultados de la compilación:</title><nl/>%1"

#: plugin_katebuild.cpp:844
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""
"Compilación de <b>%1</b> completada. %2 errores, %3 advertencias, %4 notas"

#: plugin_katebuild.cpp:850
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Se ha encontrado un error."
msgstr[1] "Se han encontrado %1 errores."

#: plugin_katebuild.cpp:854
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Se ha encontrado una advertencia."
msgstr[1] "Se han encontrado %1 advertencias."

#: plugin_katebuild.cpp:857
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Se ha encontrado una nota."
msgstr[1] "Se han encontrado %1 notas."

#: plugin_katebuild.cpp:862
#, kde-format
msgid "Build failed."
msgstr "Ha fallado la compilación."

#: plugin_katebuild.cpp:864
#, kde-format
msgid "Build completed without problems."
msgstr "Compilación finalizada sin problemas."

#: plugin_katebuild.cpp:869
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""
"Compilación de <b>%1 cancelada</b>. %2 errores, %3 advertencias, %4 notas"

#: plugin_katebuild.cpp:893
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "No se puede ejecutar: %1. No se ha definido un directorio de trabajo."

#: plugin_katebuild.cpp:1119
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "error"

#: plugin_katebuild.cpp:1122
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "advertencia"

#: plugin_katebuild.cpp:1125
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "nota|info"

#: plugin_katebuild.cpp:1128
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "referencia no definida"

#: plugin_katebuild.cpp:1161
#, kde-format
msgid "Target Set"
msgstr "Conjunto de objetivos"

#: plugin_katebuild.cpp:1163
#, kde-format
msgid "Clean"
msgstr "Limpiar"

#: plugin_katebuild.cpp:1164
#, kde-format
msgid "Config"
msgstr "Configuración"

#: plugin_katebuild.cpp:1165
#, kde-format
msgid "ConfigClean"
msgstr "ConfigClean"

#: plugin_katebuild.cpp:1285
#, kde-format
msgid "build"
msgstr "compilar"

#: plugin_katebuild.cpp:1288
#, kde-format
msgid "clean"
msgstr "limpiar"

#: plugin_katebuild.cpp:1291
#, kde-format
msgid "quick"
msgstr "rápido"

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>T:</B> %1"

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Dir:</B> %1"

#: TargetHtmlDelegate.cpp:98
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Déjelo en blanco para usar el directorio del documento actual.\n"
"Añada directorios de búsqueda separando las rutas con «;»"

#: TargetHtmlDelegate.cpp:102
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Use:\n"
"«%f» para el archivo actual\n"
"«%d» para el directorio del archivo actual\n"
"«%n» para el nombre del archivo actual sin sufijo"

#: TargetModel.cpp:388
#, kde-format
msgid "Command/Target-set Name"
msgstr "Nombre del conjunto de órdenes u objetivos"

#: TargetModel.cpp:391
#, kde-format
msgid "Working Directory / Command"
msgstr "Orden o directorio de trabajo"

#: TargetModel.cpp:394
#, kde-format
msgid "Run Command"
msgstr "Ejecutar orden"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""
"Filtre objetivos, use las flechas para seleccionar, Intro para ejecutar"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Crear un nuevo conjunto de objetivos"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Copiar orden o conjunto de objetivos"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Borrar el objetivo actual o el conjunto actual de objetivos"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Añadir nuevo objetivo"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Objetivo seleccionado para compilar"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Compilar y ejecutar el objetivo seleccionado"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Subir el objetivo seleccionado"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Bajar el objetivo seleccionado"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Compilar"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Insertar ruta"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Seleccionar el directorio a insertar"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Compilación de <b>%1</b> completa."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "La compilación de <b>%1</b> tuvo errores."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "La compilación de <b>%1</b> tuvo advertencias."

#~ msgid "Show:"
#~ msgstr "Mostrar:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Archivo"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Línea"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Mensaje"

#~ msgid "Next Error"
#~ msgstr "Error siguiente"

#~ msgid "Previous Error"
#~ msgstr "Error anterior"

#~ msgid "Show Marks"
#~ msgstr "Mostrar marcas"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>No se ha podido abrir el archivo:</title><nl/>%1<br/>Pruebe a "
#~ "añadir una ruta de búsqueda al directorio de trabajo en las preferencias "
#~ "del objetivo."

#~ msgid "Error"
#~ msgstr "Error"

#~ msgid "Warning"
#~ msgstr "Advertencia"

#~ msgid "Only Errors"
#~ msgstr "Solo errores"

#~ msgid "Errors and Warnings"
#~ msgstr "Errores y advertencias"

#~ msgid "Parsed Output"
#~ msgstr "Salida del análisis"

#~ msgid "Full Output"
#~ msgstr "Salida completa"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr ""
#~ "Marque la casilla para hacer que la orden sea la predeterminada para el "
#~ "conjunto de objetivos."

#~ msgid "Select active target set"
#~ msgstr "Seleccionar el conjunto de objetivos activo"

#~ msgid "Filter targets"
#~ msgstr "Filtrar objetivos"

#~ msgid "Build Default Target"
#~ msgstr "Compilar el objetivo por omisión"

#~ msgid "Build and Run Default Target"
#~ msgstr "Compilar y ejecutar el objetivo por omisión"

#~ msgid "Build Previous Target"
#~ msgstr "Compilar el objetivo anterior"

#~ msgid "Active target-set:"
#~ msgstr "Conjunto de objetivos activo:"

#~ msgid "config"
#~ msgstr "configurar"

#~ msgid "Kate Build Plugin"
#~ msgstr "Complemento de compilación para Kate"

#~ msgid "Select build target"
#~ msgstr "Seleccionar objetivo a compilar"

#~ msgid "Filter"
#~ msgstr "Filtro"

#~ msgid "Build Output"
#~ msgstr "Salida de la compilación"

#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>No se ha podido abrir el archivo:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Siguiente conjunto de destinos"

#~ msgid "No previous target to build."
#~ msgstr "No había destino anterior para compilar."

#~ msgid "No target set as default target."
#~ msgstr "No se ha establecido ningún destino como predeterminado."

#~ msgid "No target set as clean target."
#~ msgstr "No se ha establecido ningún destino como limpio."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "No se ha encontrado el destino «%1» que se iba a compilar."

#~ msgid "Really delete target %1?"
#~ msgstr "¿Realmente desea borrar el destino %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Aún no se ha compilado nada."

#~ msgid "Target Set %1"
#~ msgstr "Conjunto de destinos %1"

#~ msgid "Target"
#~ msgstr "Destino"

#~ msgid "Target:"
#~ msgstr "Destino:"

#~ msgid "from"
#~ msgstr "de"

#~ msgid "Sets of Targets"
#~ msgstr "Conjuntos de destinos"

#~ msgid "Make Results"
#~ msgstr "Resultados de Make"

#~ msgid "Others"
#~ msgstr "Otros"

#~ msgid "Quick Compile"
#~ msgstr "Compilación rápida"

#~ msgid "The custom command is empty."
#~ msgstr "La orden personalizada está vacía."

#~ msgid "New"
#~ msgstr "Nuevo"

#~ msgid "Copy"
#~ msgstr "Copiar"

#~ msgid "Delete"
#~ msgstr "Eliminar"

#~ msgid "Quick compile"
#~ msgstr "Compilación rápida"
